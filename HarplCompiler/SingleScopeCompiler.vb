Imports Harpl.Compiler.ByteCode
Imports Harpl.Compiler.ExpressionCompiler
Imports Harpl.Compiler.Results
Imports Harpl.Compiler.Tokenizer

Class SingleScopeCompiler
    ReadOnly _document As ParsedDocument
    ReadOnly _result As AssemblyCompilationResult
    ReadOnly _expressionCompiler As ExpressionCompiler.ExpressionCompiler
    Public CompileTimeSemanter As Semanter
    Sub New(parsedDoc As ParsedDocument, result As AssemblyCompilationResult, compileTimeSemanter As Semanter)
        _result = result
        _document = parsedDoc
        Me.compileTimeSemanter = compileTimeSemanter
        _expressionCompiler = New ExpressionCompiler.ExpressionCompiler(_document, _result, compileTimeSemanter)
    End Sub

    Dim _index As Integer = 0
    Public Sub Compile()
        DigestNonUsefulTokens()
        While Not EndOfTokens()
            Select Case CurrentToken.Kind
                Case TokenKind.Declaration : CompileDeclaration()
                Case TokenKind.Output : CompileOutput()
                Case Else
                    _result.AddError($"Unexpected token {CurrentToken.Text}", CurrentToken.SourceLocation)
            End Select
            DigestNonUsefulTokens()
        End While
        _result.ReduceErrorsToFirstMaximumDistinctErrors()
    End Sub

    Sub CompileOutput()
        MoveToNextUsefulToken()
        If Not EndOfTokens() Then
            _expressionCompiler.CompileExpression(_index)
            _result.GeneratedIntermediateCode.Code.Add(HarplBytecode.OUTPUT.ToString)
            WriteReadVariableCode(_expressionCompiler.GetResultToken)
            _index = _expressionCompiler.GetNextIndex
        End If
    End Sub

    Private Sub MoveNextToken()
        _index += 1
    End Sub

    Private Function MoveToNextUsefulToken() As Token
        MoveNextToken()
        DigestNonUsefulTokens()
        If Not EndOfTokens() Then Return CurrentToken() Else Return Nothing
    End Function

    Private Sub DigestNonUsefulTokens()
        While Not EndOfTokens() AndAlso (CurrentToken.Kind = TokenKind.Comment OrElse CurrentToken.Kind = TokenKind.Carrier)
            MoveNextToken()
        End While
    End Sub

    Function EndOfTokens() As Boolean
        Return _index >= _document.Tokens.Length
    End Function

    Function CurrentToken() As Token
        Return _document.Tokens(_index)
    End Function

    Sub CompileDeclaration()
        Dim errToken = CurrentToken()
        MoveToNextUsefulToken()
        If EndOfTokens() Then
            _result.AddError("Unexpected end of source code", errToken.SourceLocation)
        ElseIf CurrentToken.Kind = TokenKind.UnsemantedIdentifier Then
            GenerateVariableCompilationByteCode(errToken)
        Else
            _result.AddError($"Unexpected token {CurrentToken.Text}", CurrentToken.SourceLocation)

        End If
    End Sub

    Private Sub GenerateVariableCompilationByteCode(errToken As Token)

        Dim varName As Token = GetVarName()
        If Not ValidateDataKindOperator() Then Return

        Dim dataKind = MoveToNextUsefulToken()

        If dataKind IsNot Nothing Then
            ValidateDataKindDefinition(varName, dataKind)

            If MoveToNextUsefulToken() Is Nothing Then Return

            If CurrentToken.Kind = TokenKind.Coma Then
                CompileDeclaration()
            ElseIf CurrentToken.Kind = TokenKind.Equal Then
                CompileVariableInitialization(errToken, varName, dataKind)
            End If

        End If
    End Sub

    Private Function GetVarName() As Token

        Dim varName = CurrentToken()
        If compileTimeSemanter.GlobalScope.GetVariableKind(varName.Text) <> HarplOPKind.None Then
            _result.AddError("Duplicate identifier.", CurrentToken.SourceLocation)
        End If
        Return varName
    End Function

    Private Sub CompileVariableInitialization(errToken As Token, varName As Token, dataKind As Token)

        If MoveToNextUsefulToken() IsNot Nothing Then
            _expressionCompiler.CompileExpression(_index)
            WriteSetInstruction(varName, dataKind, _expressionCompiler.GetResultToken)
            _index = _expressionCompiler.GetNextIndex
            If Not EndOfTokens() AndAlso CurrentToken()?.Kind = TokenKind.Coma Then
                CompileDeclaration()
            End If
        Else
            _result.AddError("Unexpected end of source code", errToken.SourceLocation)
        End If
    End Sub

    Private Sub ValidateDataKindDefinition(varName As Token, dataKind As Token)
        Select Case dataKind.Kind
            Case TokenKind.DecimalDataKind, TokenKind.StringDataKind, TokenKind.BoolDataKind
                GenerateDeclareVarCode(varName, dataKind)
            Case Else
                Throw New NotImplementedException()
        End Select
    End Sub

    Private Sub GenerateDeclareVarCode(varName As Token, dataKind As Token)
        _result.GeneratedIntermediateCode.Code.Add(HarplBytecode.ALOC.ToString)
        Select Case dataKind.Kind
            Case TokenKind.StringDataKind
                _result.GeneratedIntermediateCode.Code.Add(HarplOPKind.StringVar.ToString)
                CompileTimeSemanter.GlobalScope.RegisterVariable(varName.Text, HarplOPKind.StringVar)
            Case TokenKind.DecimalDataKind
                _result.GeneratedIntermediateCode.Code.Add(HarplOPKind.FloatVar.ToString)
                CompileTimeSemanter.GlobalScope.RegisterVariable(varName.Text, HarplOPKind.FloatVar)

            Case TokenKind.BoolDataKind
                _result.GeneratedIntermediateCode.Code.Add(HarplOPKind.BoolVar.ToString)
                CompileTimeSemanter.GlobalScope.RegisterVariable(varName.Text, HarplOPKind.BoolVar)
        End Select
        _result.GeneratedIntermediateCode.Code.Add(varName.Text)
    End Sub

    Private Sub WriteSetInstruction(targetVar As Token, targetKind As Token, result As Token)
        _result.GeneratedIntermediateCode.Code.Add(HarplBytecode.SET.ToString)
        Select Case targetKind.Kind
            Case TokenKind.DecimalDataKind
                _result.GeneratedIntermediateCode.Code.Add(HarplOPKind.FloatVar.ToString)
            Case TokenKind.StringDataKind
                _result.GeneratedIntermediateCode.Code.Add(HarplOPKind.StringVar.ToString)
            Case TokenKind.BoolDataKind
                _result.GeneratedIntermediateCode.Code.Add(HarplOPKind.BoolVar.ToString)
            Case Else
                Throw New NotImplementedException
        End Select
        _result.GeneratedIntermediateCode.Code.Add(targetVar.Text)

        WriteReadVariableCode(result)

    End Sub

    Sub WriteReadVariableCode(variable As Token)
        Dim op = _expressionCompiler.GetOp(variable)
        _result.GeneratedIntermediateCode.Code.Add(op.ToString)

        If variable.Kind = TokenKind.StringLiteral Then
            _result.GeneratedIntermediateCode.Code.Add(DirectCast(variable, StringLiteralToken).ScapedText)
        Else
            _result.GeneratedIntermediateCode.Code.Add(variable.Text)
        End If

    End Sub

    Private Function ValidateDataKindOperator() As Boolean

        Dim hasError As Boolean = False
        If MoveToNextUsefulToken() Is Nothing Then
            _result.AddError("Unexpected end of source code.", CurrentToken.SourceLocation)
            hasError = True
        ElseIf CurrentToken.Kind <> TokenKind.DataTypeOperator
            _result.AddError("Expecting :", CurrentToken.SourceLocation)
            hasError = True
        End If
        Return Not hasError
    End Function

End Class