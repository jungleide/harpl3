﻿Imports System.Diagnostics.Eventing.Reader
Imports Harpl.Compiler.ByteCode
Imports Harpl.Compiler.ExpressionCompiler
Imports Harpl.Compiler.Results
Imports Harpl.Compiler.Tokenizer

Public Class HarplCompiler

    Function Compile(source As String) As AssemblyCompilationResult

        Dim result As New AssemblyCompilationResult()
        result.GeneratedIntermediateCode = New IntermediateCode

        Dim parser As New Parser(source, result)
        parser.Tokenize()
        Dim semanter = New Semanter
        Dim singleScopeCompiler = New SingleScopeCompiler(parser.Document, result, semanter)
        singleScopeCompiler.Compile()
        'If Not result.Errors.Any Then
        'Dim runner = New StringBasedInterpreter(result.GeneratedIntermediateCode.Code.ToArray)
        'runner.SlowInterpet()
        'End If


        Return result
    End Function

    Friend CompileTimeSemanter As Semanter

End Class