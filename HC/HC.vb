﻿Imports System.IO
Imports Harpl.Compiler
Imports Harpl.Compiler.ByteCode
Imports Harpl.Compiler.Results

Module HC

    Sub WriteWelcome()
        Console.ForegroundColor = ConsoleColor.Yellow
        Console.WriteLine()
        Console.WriteLine(
"   ,ggg,        gg                                           
  dP""""Y8b       88                                     ,dPYb,
  Yb, `88       88                                     IP'`Yb
   `""  88       88                                     I8  8I
       88aaaaaaa88                                     I8  8'
       88""""""""""""""88    ,gggg,gg   ,gggggg,  gg,gggg,    I8 dP 
       88       88   dP""  ""Y8I   dP""""""""8I  I8P""  ""Yb   I8dP  
       88       88  i8'    ,8I  ,8'    8I  I8'    ,8i  I8P   
       88       Y8,,d8,   ,d8b,,dP     Y8,,I8 _  ,d8' ,d8b,_ 
       88       `Y8P""Y8888P""`Y88P      `Y8PI8 YY88888P8P'""Y88
                                           I8                
                                           I8                
                                           I8                
                                           I8                
                                           I8                
                                           I8                
")
        Console.BackgroundColor = ConsoleColor.Black
        Console.ForegroundColor = ConsoleColor.Gray
    End Sub

    Sub Main(args() As String)
        WriteWelcome()
        If args.Count < 1 Then
            Console.WriteLine("Expecting document to compile")
        Else
            Console.WriteLine("Going to compile " & args(0))
            Dim sourceCode = LoadSourceCode(args(0))
            Dim init = Now
            Dim result = Compile(sourceCode)
            Dim timeSpentInCompilation = (Now - init).ToString
            If result.Errors.Count = 0 Then
                Console.WriteLine("Compiled in approximately " & timeSpentInCompilation)
                Console.Write("Press ENTER to run the compiled program>")
                Console.ReadLine()
                RunProgram(result)
            Else
                Console.WriteLine("Could not run compile. Errors: ")
                For Each msg In result.Errors
                    Console.WriteLine("  " & msg.ToString)
                Next
            End If
        End If
        Console.Write("Press ENTER to close this>")
        Console.ReadLine()
    End Sub

    Private Sub RunProgram(result As AssemblyCompilationResult)
        Dim runner As New StringBasedInterpreter()
        runner.ByteCode = result.GeneratedIntermediateCode.Code
        runner.Run()
    End Sub

    Private Function Compile(sourceCode As String) As AssemblyCompilationResult
        Dim compiler As New HarplCompiler
        Return compiler.Compile(sourceCode)
    End Function

    Private Function LoadSourceCode(sourcePath As String) As String
        Dim sourceDocument = New FileStream(sourcePath, FileMode.Open, FileAccess.Read)
        Dim documentReader = New StreamReader(sourceDocument)
        Dim sourceCode = documentReader.ReadToEnd()
        documentReader.Close()
        sourceDocument.Close()
        Return sourceCode
    End Function
End Module
