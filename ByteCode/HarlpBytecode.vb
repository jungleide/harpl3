﻿' ALOC
'       VN | VS
'       VARNAME

'Example: (Suma la variable numérica MyVar + 34 y lo pone en el registro temporal 0)
' ADD
'   VN
'   MyVar
'   LN
'   34
'   TN
'   0

'SET
'   (target VN | VS + name)
'   (source VN | VS | TN, etc...)

Public Enum HarplBytecode
    ' ReSharper disable InconsistentNaming
    ADD
    SUBS
    MULTY
    DIV
    STRC
    ALOC
    [SET]
    USUB
    UNOT
    EQUAL
    GEQUAL
    GREATER
    SEQUAL
    SMALLER
    DIFFERENT
    OUTPUT
    INPUT
    ' ReSharper restore InconsistentNaming
End Enum

