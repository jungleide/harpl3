﻿' ReSharper disable InconsistentNaming
Public Enum HarplOPKind
    None = 0
    StrLiteral = 1
    TmpString = 8
    StringVar = 32
    FloatLiteral = 2
    TemporaryFloat = 4
    FloatVar = 16
    BoolLiteral = 64
    TmpBool = 128
    BoolVar = 256

End Enum
' ReSharper restore InconsistentNaming
