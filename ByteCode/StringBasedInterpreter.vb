Imports System.Globalization

Public Class StringBasedInterpreter
    Private _code As String()
    Public ReadOnly Property GlobalDataScope As DataScope
    Sub New()
        GlobalDataScope = New DataScope
    End Sub

    Public Property ByteCode As IEnumerable(Of String)
        Get
            Return _code
        End Get
        Set
            _code = Value.ToArray()
        End Set
    End Property

    Dim _index As Integer = 0

    Public Sub Run()
        _index = 0
        Dim done = False
        While Not done And _index < _code.Length
            Select Case _code(_index)

                Case HarplBytecode.ALOC.ToString() : AlocVariable()

                Case HarplBytecode.SET.ToString() : SetVariable()

                Case HarplBytecode.ADD.ToString() : Calculate(Function(l, r) l + r)

                Case HarplBytecode.SUBS.ToString() : Calculate(Function(l, r) l - r)

                Case HarplBytecode.MULTY.ToString() : Calculate(Function(l, r) l * r)

                Case HarplBytecode.DIV.ToString() : Calculate(Function(l, r) l / r)

                Case HarplBytecode.STRC.ToString() : CalculateConcatenation()

                Case HarplBytecode.USUB.ToString() : CalculateUnnarySubstraction()

                Case HarplBytecode.UNOT.ToString() : CalculateUnaryNot()

                Case HarplBytecode.EQUAL.ToString() : CompareValues(Function(nl, nr) nl = nr, Function(sl, sr) sl = sr, Function(bl, br) bl = br)

                Case HarplBytecode.GREATER.ToString() : CompareValues(Function(nl, nr) nl > nr, Function(sl, sr) sl > sr, Function(bl, br) bl > br)

                Case HarplBytecode.GEQUAL.ToString() : CompareValues(Function(nl, nr) nl >= nr, Function(sl, sr) sl >= sr, Function(bl, br) bl >= br)

                Case HarplBytecode.SMALLER.ToString() : CompareValues(Function(nl, nr) nl < nr, Function(sl, sr) sl < sr, Function(bl, br) bl < br)

                Case HarplBytecode.SEQUAL.ToString() : CompareValues(Function(nl, nr) nl <= nr, Function(sl, sr) sl <= sr, Function(bl, br) bl <= br)

                Case HarplBytecode.DIFFERENT.ToString() : CompareValues(Function(nl, nr) nl <> nr, Function(sl, sr) sl <> sr, Function(bl, br) bl <> br)

                Case HarplBytecode.OUTPUT.ToString : PerformOutput()

                Case Else
                    Throw New Exception("Can't understand bytecode, which is quite weird because I've written it myself")
            End Select
        End While
    End Sub


    Sub PerformOutput()
        _index += 1
        Dim output As String = Nothing
        Select Case _code(_index)
            Case HarplOPKind.BoolVar.ToString, HarplOPKind.BoolLiteral.ToString, HarplOPKind.TmpBool.ToString
                output = GetBoolean().ToString
            Case HarplOPKind.StrLiteral.ToString, HarplOPKind.TmpString.ToString, HarplOPKind.StringVar.ToString()
                output = GetString()
            Case HarplOPKind.FloatVar.ToString, HarplOPKind.FloatLiteral.ToString, HarplOPKind.TemporaryFloat.ToString()
                output = GetNumber().ToString
            Case Else
                Throw New NotImplementedException
        End Select
        Console.WriteLine(output)
    End Sub

    Private Sub CalculateUnnarySubstraction()
        _index += 1
        Dim num = -GetNumber()
        SetNumber(num)
    End Sub

    Private Sub CalculateUnaryNot()
        _index += 1
        Dim bool = Not GetBoolean()
        SetBool(bool)

    End Sub

    Private Sub SetVariable()

        _index += 1
        Select Case _code(_index)
            Case HarplOPKind.FloatVar.ToString
                _index += 1
                Dim varName = _code(_index)
                _index += 1
                Dim value = GetNumber()
                GlobalDataScope.Floats(varName) = value

            Case HarplOPKind.StringVar.ToString
                _index += 1
                Dim varName = _code(_index)
                _index += 1
                Dim value = GetString()
                GlobalDataScope.Strings(varName) = value

            Case HarplOPKind.BoolVar.ToString()
                _index += 1
                Dim varName = _code(_index)
                _index += 1
                Dim value = GetBoolean()
                GlobalDataScope.Bools(varName) = value

            Case Else
                Throw New NotImplementedException


        End Select
    End Sub

    Private Sub AlocVariable()

        _index += 1 : Dim kind = _code(_index)
        _index += 1 : Dim name = _code(_index)
        Select Case kind
            Case HarplOPKind.FloatVar.ToString
                GlobalDataScope.Floats(name) = 0
            Case HarplOPKind.StringVar.ToString
                GlobalDataScope.Strings(name) = String.Empty
        End Select
        _index += 1
    End Sub

    Private Sub CalculateConcatenation()

        _index += 1
        Dim left = GetString()
        Dim right = GetString()
        SetString(left & right)

    End Sub

    Private Sub Calculate(calculation As Func(Of Double, Double, Double))
        _index += 1
        Dim left = GetNumber()
        Dim right = GetNumber()
        Dim result = calculation(left, right)
        SetNumber(result)
    End Sub

    Private Sub CompareValues(numComp As Func(Of Double, Double, Boolean),
                              strComp As Func(Of String, String, Boolean),
                              boolComp As Func(Of Boolean, Boolean, Boolean))
        Select Case _code(_index + 1)

            Case HarplOPKind.BoolVar.ToString, HarplOPKind.BoolLiteral.ToString, HarplOPKind.TmpBool.ToString
                CompareBoolValues(boolComp)

            Case HarplOPKind.TmpString.ToString, HarplOPKind.StringVar.ToString, HarplOPKind.StrLiteral.ToString
                CompareStringValues(strComp)

            Case HarplOPKind.FloatVar.ToString, HarplOPKind.FloatLiteral.ToString, HarplOPKind.TemporaryFloat.ToString
                CompareNumericalValues(numComp)

        End Select
    End Sub

    Private Sub CompareNumericalValues(compFloat As Func(Of Double, Double, Boolean))
        _index += 1
        Dim left = GetNumber()
        Dim right = GetNumber()
        Dim result = compFloat(left, right)
        SetBool(result)
    End Sub

    Private Sub CompareStringValues(compString As Func(Of String, String, Boolean))
        _index += 1
        Dim left = GetString()
        Dim right = GetString()
        Dim result = compString(left, right)
        SetBool(result)
    End Sub

    Private Sub CompareBoolValues(compBool As Func(Of Boolean, Boolean, Boolean))
        _index += 1
        Dim left = GetBoolean()
        Dim right = GetBoolean()
        Dim result = compBool(left, right)
        SetBool(result)

    End Sub

    Private Function GetNumber() As Double
        Dim number As Double
        Dim code = _code(_index)
        _index += 1
        Select Case code

            Case HarplOPKind.FloatLiteral.ToString()
                number = Utils.StrToDouble(_code(_index))
                _index += 1

            Case HarplOPKind.TemporaryFloat.ToString()
                number = _numRegisters(CInt(_code(_index)))
                _index += 1

            Case HarplOPKind.FloatVar.ToString()
                number = GlobalDataScope.Floats(_code(_index))
                _index += 1

            Case Else
                Throw New NotImplementedException
        End Select

        Return number
    End Function

    Private Function GetBoolean() As Boolean
        Dim bool As Boolean
        Dim code = _code(_index)
        _index += 1

        Select Case code

            Case HarplOPKind.FloatLiteral.ToString()
                bool = Utils.StrToDouble(_code(_index)) <> 0
                Throw New Exception("Can't convert a string to a bool")

            Case HarplOPKind.TemporaryFloat.ToString()
                bool = _numRegisters(CInt(_code(_index))) <> 0
                _index += 1

            Case HarplOPKind.FloatVar.ToString()
                bool = GlobalDataScope.Floats(_code(_index)) <> 0
                _index += 1

            Case HarplOPKind.BoolVar.ToString()
                bool = GlobalDataScope.Bools(_code(_index))
                _index += 1

            Case HarplOPKind.BoolLiteral.ToString()
                bool = String.Compare(_code(_index), "True", StringComparison.InvariantCultureIgnoreCase) = 0
                _index += 1

            Case HarplOPKind.TmpBool.ToString()
                bool = _boolRegisters(CInt(_code(_index)))
                _index += 1

            Case Else
                Throw New NotImplementedException
        End Select

        Return bool

    End Function

    Private Function GetString() As String
        Dim myString As String
        Dim code = _code(_index)
        _index += 1
        Select Case code
            Case HarplOPKind.FloatLiteral.ToString(), HarplOPKind.StrLiteral.ToString, HarplOPKind.BoolLiteral.ToString
                myString = _code(_index)
                _index += 1

            Case HarplOPKind.TemporaryFloat.ToString()
                myString = CType(_numRegisters(CInt(_code(_index))), String)
                _index += 1

            Case HarplOPKind.FloatVar.ToString()
                Dim i = GlobalDataScope.Floats(_code(_index))
                myString = CType(i, String)
                _index += 1

            Case HarplOPKind.TmpString.ToString
                myString = _numStrings(CInt(_code(_index)))
                _index += 1

            Case HarplOPKind.StringVar.ToString
                myString = GlobalDataScope.Strings(_code(_index))
                _index += 1

            Case HarplOPKind.BoolVar.ToString()
                myString = GlobalDataScope.Bools(_code(_index)).ToString
                _index += 1

            Case HarplOPKind.TmpBool.ToString()
                myString = _boolRegisters(CInt(_code(_index))).ToString
                _index += 1

            Case Else
                Throw New NotImplementedException

        End Select

        Return myString
    End Function

    Private Sub SetNumber(value As Double)
        Dim code = _code(_index)
        _index += 1
        Select Case code

            Case HarplOPKind.TemporaryFloat.ToString()
                _numRegisters(CInt(_code(_index))) = value
                _index += 1

            Case HarplOPKind.FloatVar.ToString
                GlobalDataScope.Floats((_code(_index))) = value
                _index += 1

            Case Else
                Throw New NotImplementedException
        End Select

    End Sub

    Private Sub SetBool(value As Boolean)
        Dim code = _code(_index)
        _index += 1
        Select Case code

            Case HarplOPKind.TmpBool.ToString()
                _boolRegisters(CInt(_code(_index))) = value
                _index += 1

            Case HarplOPKind.BoolVar.ToString
                GlobalDataScope.Bools((_code(_index))) = value
                _index += 1

            Case Else
                Throw New NotImplementedException
        End Select
    End Sub

    Private Sub SetString(value As String)
        Dim code = _code(_index)
        _index += 1
        Select Case code
            Case HarplOPKind.TmpString.ToString
                _numStrings(CInt(_code(_index))) = value
                _index += 1
            Case HarplOPKind.StringVar.ToString()
                GlobalDataScope.Strings(_code(_index)) = value
                _index += 1

            Case Else
                Throw New NotImplementedException
        End Select

    End Sub
    ReadOnly _numRegisters(1024) As Double
    ReadOnly _numStrings(1024) As String
    ReadOnly _boolRegisters(1024) As Boolean

End Class

Public Class DataScope
    Public ReadOnly Strings As New Dictionary(Of String, String)
    Public ReadOnly Floats As New Dictionary(Of String, Double)
    Public ReadOnly Bools As New Dictionary(Of String, Boolean)

    Public Sub Clear()
        Floats.Clear()
        Strings.Clear()
        Bools.Clear()
    End Sub
End Class