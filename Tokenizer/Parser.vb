﻿Imports System.Text
Imports Harpl.Compiler.Results

Public Class Parser

    ReadOnly _source As String

    Public Document As ParsedDocument
    Public CompilationResult As AssemblyCompilationResult

    Sub New(source As String, result As AssemblyCompilationResult)
        _source = source
        _solver = New OperatorSolver(source)
        CompilationResult = result
    End Sub

    Dim _index As Integer = 0
    Dim _curLine As Integer = 0
    Dim _curChar As Integer = 0

    ReadOnly _solver As OperatorSolver

    Public Sub Tokenize()
        _parsedDocument.Clear()
        While Not EndOfSource()
            RemoveWhiteSpaces()
            If Not EndOfSource() Then ParseCurrentToken()

        End While
        Document = New ParsedDocument(_parsedDocument)
    End Sub

    Private Sub DefineIdentifierTokenKind(token As Token)
        token.Kind = TokenKind.UnsemantedIdentifier
        Select Case token
            Case "Def"
                token.Kind = TokenKind.Declaration

            Case "Float"
                token.Kind = TokenKind.DecimalDataKind

            Case "String"
                token.Kind = TokenKind.StringDataKind

            Case "Bool"
                token.Kind = TokenKind.BoolDataKind

            Case "True", "False"
                token.Kind = TokenKind.BoolLiteral

            Case "Output"
                token.Kind = TokenKind.Output

            Case "Input"
                token.Kind = TokenKind.Input

            Case Else
                token.Kind = TokenKind.UnsemantedIdentifier
        End Select
    End Sub

    Private Sub ParseCurrentToken()

        Select Case CurrentChar()

            Case "a"c To "z"c, "A"c To "Z"c, "_"c
                Dim token = ParseTokenUntilStopCondition(AddressOf StopParsingIDToken)
                DefineIdentifierTokenKind(token)

            Case "0"c To "9"c

                Dim numberHasDot = False
                ParseTokenUntilStopCondition(Function() StopParsingNumericToken(numberHasDot)).Kind = TokenKind.Number

            Case ChrW(13), ChrW(10)
                ParseTokenUntilStopCondition(AddressOf StopParsingCarrierReturnToken).Kind = TokenKind.Carrier

            Case "\"c
                ParseTokenUntilStopCondition(AddressOf StopParsingCommentToken).Kind = TokenKind.Comment

            Case """"c
                ParseStringLiteral()

            Case "+"c, "-"c, "*"c, "/"c, "&"c, ">"c, "<"c, "="c, "!"c, "|"c, "("c, ")"c, ","c, ":"c
                ParseOperator()

            Case Else
                Dim token = ParseTokenUntilStopCondition(AddressOf StopParsingErrorToken)
                token.Kind = TokenKind.TokenError
                CompilationResult.AddError($"Unexpected token '{token.Text}'", token.SourceLocation)
        End Select

    End Sub

    Private Sub ParseOperator()
        _solver.SolveOperator(_index)
        Dim T As New Token(_source, _index, _solver.OperatorLength, New TextLocation(_curLine, _curChar))
        For i = 0 To _solver.OperatorLength - 1
            NextChar()
        Next
        T.Kind = _solver.ObtainedOperator
        _parsedDocument.Add(T)
    End Sub

    Private Function StopParsingIDToken() As Boolean
        Dim done As Boolean
        Select Case CurrentChar()
            Case "a"c To "z"c, "A"c To "Z"c, "_"c, "0"c To "9"c
                done = False
            Case Else
                done = True
        End Select
        Return done
    End Function

    Private Function StopParsingNumericToken(ByRef numberHasDot As Boolean) As Boolean
        Dim done As Boolean
        Select Case CurrentChar()
            Case "0"c To "9"c
                done = False
            Case "."c
                If Not numberHasDot Then
                    numberHasDot = True
                    done = False
                Else
                    done = True
                End If

            Case Else
                done = True
        End Select
        Return done
    End Function

    Private Function StopParsingErrorToken() As Boolean
        Dim done As Boolean
        Select Case CurrentChar()
            Case ChrW(13), ChrW(10), " "c, ChrW(9)
                done = True
            Case Else
                done = False
        End Select
        Return done
    End Function

    Private Function StopParsingCarrierReturnToken() As Boolean
        Select Case CurrentChar()
            Case ChrW(13), ChrW(10), " "c, ChrW(9) : Return False
            Case Else : Return True
        End Select
    End Function

    Private Function StopParsingCommentToken() As Boolean
        Select Case CurrentChar()
            Case ChrW(13), ChrW(10) : Return True
            Case Else : Return False
        End Select
    End Function

    Private Function ParseTokenUntilStopCondition(stopParsingCriteria As Func(Of Boolean)) As Token
        Dim initialIndex As Integer = _index
        Dim location = New TextLocation(_curLine, _curChar)
        Dim done = False
        While Not done AndAlso Not EndOfSource()
            done = stopParsingCriteria.Invoke()
            If Not done Then NextChar()
        End While
        Dim T As New Token(_source, initialIndex, _index - initialIndex, location)
        _parsedDocument.Add(T)
        Return T
    End Function

    <DebuggerStepThrough()>
    Private Sub RemoveWhiteSpaces()
        While Not EndOfSource() AndAlso CurrentChatIsWhiteSpace()
            NextChar()
        End While
    End Sub

    <DebuggerStepThrough()>
    Private Function CurrentChatIsWhiteSpace() As Boolean
        Return _source(_index) = " "c OrElse _source(_index) = ChrW(9)
    End Function

    Private Sub NextChar()
        _index += 1
        _curChar += 1
        If Not EndOfSource() Then
            Dim cur = CurrentChar()
            Dim prev = _source(_index - 1)

            If (cur = ChrW(13) OrElse cur = ChrW(10)) Then
                If (cur <> prev AndAlso prev <> ChrW(10) AndAlso prev <> ChrW(13)) Then
                    _curLine += 1
                End If
                _curChar = -1   'Los CR no se cuentan
            End If
        End If
    End Sub

    <DebuggerStepThrough()>
    Private Function EndOfSource() As Boolean
        Return _index >= _source.Length
    End Function

    <DebuggerStepThrough()>
    Private Function CurrentChar() As Char
        Return _source(_index)
    End Function

    Private ReadOnly _parsedDocument As New List(Of Token)

    ' ReSharper disable once UnusedMethodReturnValue.Local
    Private Function ParseStringLiteral() As Token
        Dim initialIndex As Integer = _index
        Dim location = New TextLocation(_curLine, _curChar)
        Dim done = False
        Dim scapeNext = False
        Dim scapedText As New StringBuilder(64)
        NextChar()
        While Not done AndAlso Not EndOfSource()
            Select Case CurrentChar()
                Case "~"c
                    scapeNext = ProcessTildeScapingOnLiteral(scapeNext, scapedText)
                Case """"c
                    done = ProcessQuotesInsideStringLiteral(scapeNext, scapedText)
                    scapeNext = False
                Case Else
                    scapedText.Append(CurrentChar)
                    scapeNext = False
            End Select
            If Not done Then NextChar()
        End While
        Dim T As New StringLiteralToken(_source, initialIndex, _index - initialIndex, location)
        T.ScapedText = scapedText.ToString
        _parsedDocument.Add(T)
        Return T
    End Function

    Private Function ProcessQuotesInsideStringLiteral(scapeNext As Boolean, scapedText As StringBuilder) As Boolean
        Dim done = False
        If scapeNext Then
            scapedText.Append(CurrentChar)
        Else
            NextChar()
            done = True
        End If
        Return done
    End Function

    Private Function ProcessTildeScapingOnLiteral(scapeNext As Boolean, scapedText As StringBuilder) As Boolean
        If scapeNext = False Then
            scapeNext = True
        Else
            scapedText.Append(CurrentChar)
            scapeNext = False
        End If

        Return scapeNext
    End Function

End Class