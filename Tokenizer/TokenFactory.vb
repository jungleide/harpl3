﻿Imports Harpl.Compiler.Results

Public Class TokenFactory
    Public Property Source As String
    Function CreateToken(tokenKind As TokenKind, offset As Integer, length As Integer, location As TextLocation) As Token
        Select Case tokenKind
            Case TokenKind.StringLiteral
                Return New StringLiteralToken(Source, offset, length, location)
            Case Else
                Return New Token(Source, offset, length, location)
        End Select
    End Function
End Class
