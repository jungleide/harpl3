﻿Imports Harpl.Compiler.Results

Public Class Token

    Public Property Kind As TokenKind
    Public ReadOnly Source As String

    Public ReadOnly Length As Integer
    Public ReadOnly Offset As Integer

    Public Overridable ReadOnly Property Text() As String
        Get
            <threadStatic> Static textAlreadyCalculated As Boolean = False
            <threadStatic> Static myText As String = String.Empty
            If textAlreadyCalculated = False Then
                textAlreadyCalculated = True
                myText = Source.Substring(Offset, Length)
            End If
            Return myText
        End Get
    End Property

    Public Function [Char](index As Integer) As Char
        Return Source(Offset + index)
    End Function

    Public ReadOnly SourceLocation As TextLocation

    Sub New(source As String, offset As Integer, length As Integer, sourceLocation As TextLocation)
        Me.Source = source
        Me.Offset = offset
        Me.Length = length
        Me.SourceLocation = sourceLocation
    End Sub

    Public Overrides Function ToString() As String
        Return """" & Text() & """" & Kind.ToString & " " & SourceLocation?.ToString
    End Function

    Public Shared Operator =(token1 As Token, token2 As Token) As Boolean
        If token1.Kind <> token2.Kind Then Return False
        If token1.Length <> token2.Length Then Return False
        For i = 0 To token1.Length
            If Char.ToLowerInvariant(token1.Char(i)) <> Char.ToLowerInvariant(token2.Char(i)) Then Return False
        Next
        Return True
    End Operator
    Public Shared Operator <>(token1 As Token, token2 As Token) As Boolean
        Return Not token1 = token2
    End Operator

    Public Shared Operator =(token1 As Token, token2 As String) As Boolean
        'If token1.Kind <> token2.Kind Then Return False
        If token1.Length <> token2.Length Then Return False
        For i = 0 To token1.Length - 1
            If Char.ToLowerInvariant(token1.Char(i)) <> Char.ToLowerInvariant(token2(i)) Then Return False
        Next
        Return True
    End Operator
    Public Shared Operator <>(token1 As Token, token2 As String) As Boolean
        Return Not token1 = token2
    End Operator




End Class