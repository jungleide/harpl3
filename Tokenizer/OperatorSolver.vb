'This is the operator solver
Class OperatorSolver
    Shared _operators As List(Of KeyValuePair(Of String, TokenKind))
    ReadOnly _source As String = ""
    Dim _index As Integer = 0
    Private Shared ReadOnly Lock As New Object
    Sub New(source As String)
        If _operators Is Nothing Then
            SyncLock Lock
                If _operators Is Nothing Then
                    Dim myVar = New List(Of KeyValuePair(Of String, TokenKind))
                    Threading.Thread.MemoryBarrier()
                    _operators = myVar
                    CreateOperatorsList()
                End If
            End SyncLock
        End If

        _source = source

    End Sub

    Private Sub CreateOperatorsList()
        _operators.Add(New KeyValuePair(Of String, TokenKind)("+", TokenKind.AddOperator))
        _operators.Add(New KeyValuePair(Of String, TokenKind)("++", TokenKind.Concatenation))
        _operators.Add(New KeyValuePair(Of String, TokenKind)("-", TokenKind.SubstractOperator))
        _operators.Add(New KeyValuePair(Of String, TokenKind)("*", TokenKind.MultiplyOperator))
        _operators.Add(New KeyValuePair(Of String, TokenKind)("/", TokenKind.DivideOperator))
        _operators.Add(New KeyValuePair(Of String, TokenKind)(">=", TokenKind.GreaterEqual))
        _operators.Add(New KeyValuePair(Of String, TokenKind)("<=", TokenKind.SmallerEqual))
        _operators.Add(New KeyValuePair(Of String, TokenKind)("=", TokenKind.Equal))
        _operators.Add(New KeyValuePair(Of String, TokenKind)("!=", TokenKind.Different))
        _operators.Add(New KeyValuePair(Of String, TokenKind)("!", TokenKind.Not))
        _operators.Add(New KeyValuePair(Of String, TokenKind)("&", TokenKind.And))
        _operators.Add(New KeyValuePair(Of String, TokenKind)("|", TokenKind.Or))
        _operators.Add(New KeyValuePair(Of String, TokenKind)("(", TokenKind.OpenParenthesis))
        _operators.Add(New KeyValuePair(Of String, TokenKind)(")", TokenKind.CloseParenthesis))
        _operators.Add(New KeyValuePair(Of String, TokenKind)(">", TokenKind.Greater))
        _operators.Add(New KeyValuePair(Of String, TokenKind)("<", TokenKind.Smaller))
        _operators.Add(New KeyValuePair(Of String, TokenKind)(",", TokenKind.Coma))
        _operators.Add(New KeyValuePair(Of String, TokenKind)(":", TokenKind.DataTypeOperator))
    End Sub

    Public Function SolveOperator(index As Integer) As TokenKind
        _index = index
        Dim potentialOperators = From I In _operators Where I.Key.StartsWith(_source(_index))
        Dim largestOperator = potentialOperators.OrderByDescending(Function(item) item.Key.Length)
        For Each i In largestOperator
            If index + i.Key.Length > _source.Length Then Continue For
            If _source.Substring(index, i.Key.Length) = i.Key Then
                OperatorLength = i.Key.Length
                ObtainedOperator = i.Value
                Return i.Value
            End If
        Next
        Return TokenKind.TokenError
    End Function

    Public Function SolveOperator2(index As Integer) As TokenKind
        _index = index
        Dim length = 1
        Dim found = False
        Dim [operator] As TokenKind
        While found = False
            ' ReSharper disable once AccessToModifiedClosure
            Dim operators = From I In _operators Where I.Key.StartsWith(_source.Substring(_index, length))
            ' ReSharper disable once PossibleMultipleEnumeration
            If Not operators.Any() Then
                Return TokenKind.TokenError
            ElseIf operators.Count = 1 Then
                found = True
                ' ReSharper disable once PossibleMultipleEnumeration
                [operator] = operators.First().Value
            Else
                length += 1
            End If
        End While
        OperatorLength = length
        ObtainedOperator = [operator]
        Return [operator]
    End Function

    Public ObtainedOperator As TokenKind
    Public OperatorLength As Integer

End Class