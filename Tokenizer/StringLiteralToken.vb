Imports Harpl.Compiler.Results

Public Class StringLiteralToken
    Inherits Token
    Sub New(source As String, offset As Integer, length As Integer, sourceLocation As TextLocation)
        MyBase.New(source, offset, length, sourceLocation)
        Kind = TokenKind.StringLiteral
    End Sub
    Public ScapedText As String
End Class