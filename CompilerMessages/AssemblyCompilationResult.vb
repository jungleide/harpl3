﻿
Imports System.Text
Imports Harpl.Compiler.ByteCode
Imports Harpl.Compiler.Results

Public Class AssemblyCompilationResult
    Const MaximumErrorsPerDoc As Integer = 128

    Sub New()
        Errors = New List(Of CompilationMessage)
    End Sub

    Public Errors As ICollection(Of CompilationMessage)

    Public GeneratedAssembly As HarplAssembly

    Public GeneratedIntermediateCode As IntermediateCode

    Public GeneratedByteCode As HarplBytecode

    Public Function CompilationSuccessful() As Boolean
        Dim hasErrors = Errors.Any(Function(i) i.Kind = CompilationMessage.MessageKind.Error)
        Return GeneratedAssembly IsNot Nothing AndAlso hasErrors = False
    End Function

    Public Sub AddError(errDescription As String, location As TextLocation)
        AddMessage(errDescription, location, CompilationMessage.MessageKind.Error)
    End Sub

    Public Sub AddWarning(errDescription As String, location As TextLocation)
        AddMessage(errDescription, location, CompilationMessage.MessageKind.Warning)
    End Sub

    Public Sub AddMessage(errDescription As String, location As TextLocation, kind As CompilationMessage.MessageKind)
        Errors.Add(New CompilationMessage() With
            {
                .Kind = kind,
                .Explanation = errDescription,
                .SourceLocation = location
            })
    End Sub

    Public Sub ReduceErrorsToFirstMaximumDistinctErrors()
        Errors = (From I In Errors Distinct).Take(MaximumErrorsPerDoc).ToList()
    End Sub

    Public Function GetErrorsDescription() As String
        Dim sb As New StringBuilder(1024)
        For Each errMsg In Errors
            sb.Append(errMsg.ToString)
            sb.Append(vbCr)
        Next
        Return sb.ToString
    End Function

End Class