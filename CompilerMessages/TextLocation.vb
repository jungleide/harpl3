Public Class TextLocation
    Public ReadOnly Line As Integer
    Public ReadOnly [Char] As Integer
    Sub New(line As Integer, [char] As Integer)
        Me.Line = line
        Me.Char = [char]
    End Sub

    Public Overrides Function ToString() As String
        Return $"(lin:{Line}, char:{[Char]})"
    End Function
End Class