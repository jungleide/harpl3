﻿
Imports Harpl.Compiler.Results

Public Class CompilationMessage
    Implements IEquatable(Of CompilationMessage)

    Public Kind As MessageKind
    Public Explanation As String
    Public TargetDocument As String
    Public SourceLocation As TextLocation

    Public Enum MessageKind
        None = 0
        Warning
        [Error]
    End Enum

    Public Overrides Function ToString() As String
        Return Kind.ToString & " " & Explanation & "{" & TargetDocument & "} " & SourceLocation.ToString
    End Function

    Public Function CompareTo(other As CompilationMessage) As Integer
        If other.Kind < Me.Kind Then
            Return 1
        ElseIf other.Kind > Me.Kind Then
            Return -1
        End If

        If other.Explanation < Me.Explanation Then
            Return 1
        ElseIf other.Explanation > Me.Explanation Then
            Return -1
        End If

        If other.TargetDocument < Me.TargetDocument Then
            Return 1
        ElseIf other.TargetDocument > Me.TargetDocument Then
            Return -1
        End If
        If other.SourceLocation?.ToString < Me.SourceLocation?.ToString Then
            Return 1
        ElseIf other.SourceLocation?.ToString < Me.SourceLocation?.ToString Then
            Return -1
        End If
        Return 0
    End Function

    Public Function EquatableEquals(other As CompilationMessage) As Boolean Implements IEquatable(Of CompilationMessage).Equals
        Return CompareTo(other) = 0
    End Function

    Public Overrides Function GetHashCode() As Integer

        Return ToString.GetHashCode
    End Function

End Class

