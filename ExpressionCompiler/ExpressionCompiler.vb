﻿Imports System.Text
Imports Harpl.Compiler.ByteCode
Imports Harpl.Compiler.Results
Imports Harpl.Compiler.Tokenizer
Imports Utils
Public Class ExpressionCompiler

    Friend Document As ParsedDocument

    Private _newIndex As Integer = 0

    ReadOnly _expressionBuilder As ExpressionTreeBuilder
    Private _expression As List(Of Token)
    Private ReadOnly _intermediateCode As IntermediateCode

    Private _temporaryNumIndex As Integer = 0
    Private _temporaryStringNumber As Integer = 0

    Friend ReadOnly CompilationResult As AssemblyCompilationResult

    ' ReSharper disable once IdentifierTypo
    Public Semanter As Semanter

    Sub New(parsedDocument As ParsedDocument, result As AssemblyCompilationResult, semanter As Semanter)
        Document = parsedDocument
        _expressionBuilder = New ExpressionTreeBuilder(Me)
        _expressionBuilder.MyTokens = parsedDocument.Tokens
        CompilationResult = result
        _intermediateCode = result.GeneratedIntermediateCode
        Me.Semanter = semanter
    End Sub

    Sub New(parentExpressionCompiler As ExpressionCompiler, isSubexpression As Boolean)
        Me.New(parentExpressionCompiler.Document, parentExpressionCompiler.CompilationResult, parentExpressionCompiler.Semanter)
        _expressionBuilder.MyTokens = parentExpressionCompiler._expression.ToArray
        _temporaryNumIndex = parentExpressionCompiler._temporaryNumIndex
        _temporaryStringNumber = parentExpressionCompiler._temporaryStringNumber
        If isSubexpression Then _expressionBuilder.IsSubexpression = True
    End Sub

    Public Sub CompileExpression(tokenIndex As Integer)
        '_initIndex = tokenIndex

        '1.- Build expression list
        _expression = _expressionBuilder.BuildTokensList(tokenIndex)
        _newIndex = _expressionBuilder.GetNextIndex

        'Parse ( )
        CompileNestedExpressions()

        CompileBinaryOperation({TokenKind.MultiplyOperator, TokenKind.DivideOperator})
        CompileBinaryOperation({TokenKind.AddOperator, TokenKind.SubstractOperator})
        CompileBinaryOperation({TokenKind.Concatenation})

        CompileBinaryOperation({TokenKind.Equal, TokenKind.Greater, TokenKind.GreaterEqual, TokenKind.Smaller, TokenKind.SmallerEqual, TokenKind.Different})

        ProcessUnaryOperator(0)

    End Sub

    Overrides Function ToString() As String
        If _expression Is Nothing Then Return MyBase.ToString
        Dim sb As New StringBuilder(1024)
        For Each i In _expression
            sb.Append(i.Text)
            sb.Append(" ")
        Next
        Return sb.ToString
    End Function

    Sub CompileNestedExpressions()
        Dim firstOpenParenthesis = GetFirstOpenParenthesis()
        While firstOpenParenthesis >= 0

            Dim exParser = New ExpressionCompiler(Me, True)
            exParser.CompileExpression(firstOpenParenthesis + 1)
            _temporaryStringNumber = exParser._temporaryStringNumber
            _temporaryNumIndex = exParser._temporaryNumIndex
            Dim newIndex = exParser.GetNextIndex()
            For i = newIndex - 1 To firstOpenParenthesis + 1 Step -1
                _expression.RemoveAt(i)
            Next
            _expression(firstOpenParenthesis) = exParser.GetResultToken

            firstOpenParenthesis = GetFirstOpenParenthesis()
        End While
    End Sub

    Public Function GetResultToken() As Token
        Return _expression(0)
    End Function

    Private Sub CompileBinaryOperation(operators As TokenKind())
        Dim tokenindex = GetFirstTokenOfKinds(operators)
        While tokenindex = 0
            ProcessUnaryOperator(0)
            tokenindex = GetFirstTokenOfKinds(operators)
        End While
        Dim result As Token
        While tokenindex > 0 AndAlso tokenindex < _expression.Count - 1
            Dim left = _expression(tokenindex - 1)
            Dim right = ProcessUnaryOperator(tokenindex + 1)
            'Right may require unnary operation processing
            Dim curOperator = _expression(tokenindex)

            If GetOp(left) = GetOp(right) AndAlso GetOp(left) = HarplOPKind.FloatLiteral Then
                result = StaticNumericEvaluation(left, right, curOperator)

            ElseIf (GetOp(left) = HarplOPKind.FloatLiteral OrElse GetOp(left) = HarplOPKind.StrLiteral) AndAlso
                (GetOp(right) = HarplOPKind.FloatLiteral OrElse GetOp(right) = HarplOPKind.StrLiteral) Then
                result = StaticStringEvaluation(left, right, curOperator)

            ElseIf GetOp(left) = GetOp(right) AndAlso GetOp(left) = HarplOPKind.BoolLiteral Then
                result = StaticBooleanEvaluation(left, right, curOperator)

            Else
                Dim resultTarget As HarplOPKind = GenerateOperationCode(curOperator)
                GenerateCodeForOperand(left)
                GenerateCodeForOperand(right)
                result = GenerateOperationTargetCode(resultTarget)

            End If

            RemoveCompiledOperandsFromBynaryOperation(tokenindex, result)

            tokenindex = GetFirstTokenOfKinds(operators)

        End While
    End Sub

    Private Function ProcessUnaryOperator(tokenindex As Integer) As Token
        If tokenindex >= _expression.Count Then
            CompilationResult.AddError("Unexpected end of expression.", _expression(tokenindex - 1).SourceLocation)
            Return Nothing
        End If
        Dim myOperator = _expression(tokenindex)
        Dim result = myOperator
        Select Case myOperator.Kind
            Case TokenKind.SubstractOperator
                result = ProcessSubstractionUnaryOperator(tokenindex, myOperator)
            Case TokenKind.AddOperator
                _expression.RemoveAt(tokenindex)
                result = ProcessUnaryOperator(tokenindex)
            ' ReSharper disable once RedundantEmptyCaseElse (easier to understand flow)
            Case TokenKind.Not
                result = ProcessNotUnaryOperator(tokenindex, myOperator)

            ' ReSharper disable once RedundantEmptyCaseElse. This is to explicitly say there nothing to do here!
            Case Else
                'No operation to do here
        End Select
        Return result
    End Function

    Private Function ProcessSubstractionUnaryOperator(tokenindex As Integer, myOperator As Token) As Token
        Dim result As Token

        If tokenindex + 1 >= _expression.Count Then
            CompilationResult.AddError("Unexpected end of expression.", _expression(tokenindex - 1).SourceLocation)
            result = myOperator
        Else
            ProcessUnaryOperator(tokenindex + 1)
            Dim secondOperator = _expression(tokenindex + 1)
            Select Case GetOp(secondOperator)
                Case HarplOPKind.FloatLiteral
                    result = StaticEvaluateSubstractUnaryOperator(tokenindex, secondOperator)

                Case HarplOPKind.TemporaryFloat, HarplOPKind.FloatVar
                    result = WriteUnaryOperationByteCode(tokenindex, myOperator)

                Case Else
                    CompilationResult.AddError("expecting numeral operand", secondOperator.SourceLocation)
                    result = secondOperator
            End Select
        End If
        Return result
    End Function

    Private Function ProcessNotUnaryOperator(tokenindex As Integer, myOperator As Token) As Token
        Dim result As Token

        If tokenindex + 1 >= _expression.Count Then
            CompilationResult.AddError("Unexpected end of expression.", _expression(tokenindex - 1).SourceLocation)
            result = myOperator
        Else
            ProcessUnaryOperator(tokenindex + 1)
            Dim secondOperator = _expression(tokenindex + 1)
            Select Case GetOp(secondOperator)
                Case HarplOPKind.FloatLiteral, HarplOPKind.TemporaryFloat, HarplOPKind.FloatVar
                    'result = StaticEvaluateSubstractUnaryOperator(tokenindex, secondOperator)
                    CompilationResult.AddError("expecting boolean operand", secondOperator.SourceLocation)
                    result = secondOperator
                Case HarplOPKind.BoolLiteral
                    result = StaticEvaluateNotOperator(tokenindex, secondOperator)

                Case HarplOPKind.TmpBool, HarplOPKind.BoolVar
                    result = WriteUnaryOperationByteCode(tokenindex, myOperator)

                Case Else
                    CompilationResult.AddError("expecting numeral operand", secondOperator.SourceLocation)
                    result = secondOperator
            End Select
        End If
        Return result
    End Function

    Private Function WriteUnaryOperationByteCode(tokenindex As Integer, myOperator As Token) As Token
        Dim result As Token

        _expression.RemoveAt(tokenindex)
        Dim operand = _expression(tokenindex)
        _expression.RemoveAt(tokenindex)
        Dim unnaryNumericalOperation = GenerateUnnaryNumericalOperator(myOperator)
        GenerateCodeForOperand(operand)
        Dim newtoken = GenerateOperationTargetCode(unnaryNumericalOperation)
        _expression.Insert(tokenindex, newtoken)
        result = newtoken
        Return result
    End Function

    Private Function StaticEvaluateSubstractUnaryOperator(tokenindex As Integer, secondOperator As Token) As Token
        Dim result As Token
        Dim resultNum = CStr(Utils.StrToDouble(secondOperator.Text) * -1)
        _expression.RemoveAt(tokenindex)
        _expression.RemoveAt(tokenindex)
        result = New Token(resultNum.ToString, 0, resultNum.ToString.Length, Nothing)
        result.Kind = TokenKind.Number
        _expression.Insert(tokenindex, result)
        Return result
    End Function

    Private Function StaticEvaluateNotOperator(tokenindex As Integer, secondOperator As Token) As Token
        Dim result As Token
        Dim boolResult = CStr(String.Compare(secondOperator.Text, "True", StringComparison.InvariantCultureIgnoreCase) <> 0)
        _expression.RemoveAt(tokenindex)
        _expression.RemoveAt(tokenindex)
        result = New Token(boolResult.ToString, 0, boolResult.ToString.Length, Nothing)
        result.Kind = TokenKind.BoolLiteral
        _expression.Insert(tokenindex, result)
        Return result
    End Function

    Private Sub RemoveCompiledOperandsFromBynaryOperation(tokenindex As Integer, result As Token)
        'this order is VERY important
        _expression.RemoveAt(tokenindex + 1)
        _expression(tokenindex) = result
        _expression.RemoveAt(tokenindex - 1)
    End Sub

    Private Function GenerateOperationTargetCode(resultTarget As HarplOPKind) As Token
        Dim result As Token

        If resultTarget = HarplOPKind.TemporaryFloat Then
            result = New TemporaryToken(_temporaryNumIndex, resultTarget)
            _temporaryNumIndex += 1
        Else
            result = New TemporaryToken(_temporaryStringNumber, resultTarget)
            _temporaryStringNumber += 1
        End If
        GenerateCodeForOperand(result)
        Return result
    End Function

    Private Function GenerateOperationCode(curOperator As Token) As HarplOPKind
        Dim resultTarget = HarplOPKind.TemporaryFloat
        Select Case curOperator.Kind
            Case TokenKind.MultiplyOperator
                GenerateIntermediateCode(HarplBytecode.MULTY)
            Case TokenKind.DivideOperator
                GenerateIntermediateCode(HarplBytecode.DIV)
            Case TokenKind.AddOperator
                GenerateIntermediateCode(HarplBytecode.ADD)
            Case TokenKind.SubstractOperator
                GenerateIntermediateCode(HarplBytecode.SUBS)
            Case TokenKind.Concatenation
                GenerateIntermediateCode(HarplBytecode.STRC)
                resultTarget = HarplOPKind.TmpString
            Case TokenKind.Equal
                GenerateIntermediateCode(HarplBytecode.EQUAL)
                resultTarget = HarplOPKind.TmpBool
            Case TokenKind.Greater
                GenerateIntermediateCode(HarplBytecode.GREATER)
                resultTarget = HarplOPKind.TmpBool
            Case TokenKind.GreaterEqual
                GenerateIntermediateCode(HarplBytecode.GEQUAL)
                resultTarget = HarplOPKind.TmpBool
            Case TokenKind.Smaller
                GenerateIntermediateCode(HarplBytecode.SMALLER)
                resultTarget = HarplOPKind.TmpBool
            Case TokenKind.SmallerEqual
                GenerateIntermediateCode(HarplBytecode.SEQUAL)
                resultTarget = HarplOPKind.TmpBool
            Case TokenKind.Different
                GenerateIntermediateCode(HarplBytecode.DIFFERENT)
                resultTarget = HarplOPKind.TmpBool

            Case Else
                Throw New NotImplementedException
        End Select

        Return resultTarget
    End Function

    Private Function GenerateUnnaryNumericalOperator(curOperator As Token) As HarplOPKind
        Dim resultTarget As HarplOPKind
        Select Case curOperator.Kind
            Case TokenKind.SubstractOperator
                GenerateIntermediateCode(HarplBytecode.USUB)
                resultTarget = HarplOPKind.TemporaryFloat
            Case TokenKind.Not : GenerateIntermediateCode(HarplBytecode.UNOT)
                resultTarget = HarplOPKind.TmpBool
            Case Else
                Throw New NotImplementedException("This unnary operator is not implemented yet")
        End Select
        Return resultTarget


    End Function

    Private Function StaticNumericEvaluation(left As Token, right As Token, curOperator As Token) As Token

        Dim resultNum As Double
        Dim resultBool As Boolean
        Dim isBool = False
        Dim result As Token
        Select Case curOperator.Kind
            Case TokenKind.MultiplyOperator : resultNum = StrToDouble(left.Text) * StrToDouble(right.Text)
            Case TokenKind.DivideOperator : resultNum = StrToDouble(left.Text) / StrToDouble(right.Text)
            Case TokenKind.AddOperator : resultNum = StrToDouble(left.Text) + StrToDouble(right.Text)
            Case TokenKind.SubstractOperator : resultNum = StrToDouble(left.Text) - StrToDouble(right.Text)
            Case TokenKind.Equal
                ' ReSharper disable once CompareOfFloatsByEqualityOperator
                resultBool = StrToDouble(left.Text) = StrToDouble(right.Text)
                isBool = True
            Case TokenKind.Greater
                resultBool = StrToDouble(left.Text) > StrToDouble(right.Text)
                isBool = True
            Case TokenKind.GreaterEqual
                resultBool = StrToDouble(left.Text) >= StrToDouble(right.Text)
                isBool = True
            Case TokenKind.Smaller
                resultBool = StrToDouble(left.Text) < StrToDouble(right.Text)
                isBool = True
            Case TokenKind.SmallerEqual
                resultBool = StrToDouble(left.Text) <= StrToDouble(right.Text)
                isBool = True
            Case TokenKind.Different
                resultBool = StrToDouble(left.Text) <> StrToDouble(right.Text)
                isBool = True

            Case TokenKind.Concatenation
                CompilationResult.AddError("Can't concatenate numeric values.", curOperator.SourceLocation)
        End Select
        If Not isBool Then
            result = New Token(resultNum.ToString, 0, resultNum.ToString.Length, Nothing)
            result.Kind = TokenKind.Number
        Else
            result = New Token(resultBool.ToString, 0, resultBool.ToString.Length, Nothing)
            result.Kind = TokenKind.BoolLiteral
        End If
        Return result
    End Function

    Private Function StaticBooleanEvaluation(left As Token, right As Token, curOperator As Token) As Token

        Dim resultBool As Boolean
        Dim result As Token
        Select Case curOperator.Kind
            Case TokenKind.Equal : resultBool = CBool(left.Text) = CBool(right.Text)
            Case TokenKind.Greater : resultBool = CBool(left.Text) > CBool(right.Text)
            Case TokenKind.GreaterEqual : resultBool = CBool(left.Text) >= CBool(right.Text)
            Case TokenKind.Smaller : resultBool = CBool(left.Text) < CBool(right.Text)
            Case TokenKind.SmallerEqual : resultBool = CBool(left.Text) <= CBool(right.Text)
            Case TokenKind.Different : resultBool = CBool(left.Text) <> CBool(right.Text)
            Case Else : CompilationResult.AddError("Operation not valid for boolean values.", curOperator.SourceLocation)
        End Select
        result = New Token(resultBool.ToString, 0, resultBool.ToString.Length, Nothing)
        result.Kind = TokenKind.BoolLiteral
        Return result
    End Function

    Private Function StaticStringEvaluation(left As Token, right As Token, curOperator As Token) As Token
        Dim generatedString As String = String.Empty
        Dim isBool = True, boolResult As Boolean = False
        Dim result As Token
        Select Case curOperator.Kind
            Case TokenKind.Concatenation
                isBool = False
                generatedString = (left.Text) & (right.Text)

            Case TokenKind.Different : boolResult = left.Text <> right.Text
            Case TokenKind.Equal : boolResult = left.Text = right.Text
            Case TokenKind.Smaller : boolResult = left.Text < right.Text
            Case TokenKind.Greater : boolResult = left.Text > right.Text
            Case TokenKind.SmallerEqual : boolResult = left.Text <= right.Text
            Case TokenKind.GreaterEqual : boolResult = left.Text >= right.Text
            Case Else
                CompilationResult.AddError("Unexpected operation between string literals", curOperator.SourceLocation)
        End Select
        If Not isBool Then
            result = New StringLiteralToken(generatedString, 0, generatedString.Length, Nothing)
            DirectCast(result, StringLiteralToken).ScapedText = generatedString
            result.Kind = TokenKind.StringLiteral
        Else
            result = New Token(boolResult.ToString, 0, boolResult.ToString.Length, Nothing)
            result.Kind = TokenKind.BoolLiteral
        End If
        Return result
    End Function

    Private Function GetFirstTokenOfKinds(kind() As TokenKind) As Integer
        For i = 0 To _expression.Count() - 1
            Dim aux = i
            If kind.Any(Function(item) item = _expression(aux).Kind) Then Return i
        Next
        Return -1
    End Function

    Private Function GetFirstOpenParenthesis() As Integer
        For i = 0 To _expression.Count() - 1
            If _expression(i).Kind = TokenKind.OpenParenthesis Then Return i
        Next
        Return -1
    End Function

    Public Function GetOp(token As Token) As HarplOPKind
        Select Case token.Kind
            Case TokenKind.Number : Return HarplOPKind.FloatLiteral
            Case TokenKind.StringLiteral : Return HarplOPKind.StrLiteral
            Case TokenKind.BoolLiteral : Return HarplOPKind.BoolLiteral
            Case TokenKind.InternalNum : Return HarplOPKind.TemporaryFloat
            Case TokenKind.InternalString : Return HarplOPKind.TmpString
            Case TokenKind.InternalBool : Return HarplOPKind.TmpBool
            Case TokenKind.UnsemantedIdentifier
                Dim kind = Semanter.GlobalScope.GetVariableKind(token.Text)
                If kind = HarplOPKind.None Then
                    CompilationResult.AddError($"Unknown identifier {token.Text}", token.SourceLocation)
                End If
                Return kind
            Case Else
                Throw New ArgumentException("token kind not supported")
        End Select
    End Function

    Private Sub GenerateIntermediateCode(instruction As HarplBytecode)
        _intermediateCode.Code.Add(instruction.ToString)
    End Sub

    Private Sub GenerateCodeForOperand(operand As Token)
        Dim opKind As HarplOPKind = GetOp(operand)
        Select Case opKind

            Case HarplOPKind.FloatLiteral, HarplOPKind.TemporaryFloat, HarplOPKind.TmpString, HarplOPKind.StringVar, HarplOPKind.FloatVar,
                 HarplOPKind.BoolVar, HarplOPKind.BoolLiteral, HarplOPKind.TmpBool
                _intermediateCode.Code.Add(opKind.ToString)
                _intermediateCode.Code.Add(operand.Text())

            Case HarplOPKind.StrLiteral
                _intermediateCode.Code.Add(HarplOPKind.StrLiteral.ToString)
                Dim myString = TryCast(operand, StringLiteralToken)
                _intermediateCode.Code.Add(myString.ScapedText)


            Case Else
                CompilationResult.AddError($"Unexpected operand kind", operand.SourceLocation)

        End Select

    End Sub

    Public Function GetNextIndex() As Integer
        Return _newIndex
    End Function

    Public Class TemporaryToken
        Inherits Token

        Sub New(index As Integer, kind As HarplOPKind)
            MyBase.New("", 0, 0, Nothing)
            Text = CType(index, String)
            Select Case kind

                Case HarplOPKind.TemporaryFloat
                    Me.Kind = TokenKind.InternalNum

                Case HarplOPKind.TmpString
                    Me.Kind = TokenKind.InternalString

                Case HarplOPKind.TmpBool
                    Me.Kind = TokenKind.InternalBool

                Case Else
                    Throw (New NotImplementedException)
            End Select
        End Sub

        Public Overrides ReadOnly Property Text As String

        Public Overrides Function ToString() As String
            Return MyBase.ToString
        End Function

    End Class

    Public Sub ExpressionCompilationEnded()
        _temporaryNumIndex = 0
        _temporaryStringNumber = 0
    End Sub

End Class