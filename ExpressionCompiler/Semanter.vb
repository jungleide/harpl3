Imports Harpl.Compiler.ByteCode

Public Class Semanter
    Class Scope
        Public Strings As New HashSet(Of String)
        Public Floats As New HashSet(Of String)
        Public Booleans As New HashSet(Of String)

        Public Function RegisterVariable(Name As String, kind As HarplOPKind) As Boolean
            If Strings.Contains(Name) Or Floats.Contains(Name) Then Return False
            Dim success = True
            Select Case kind
                Case HarplOPKind.FloatVar
                    Floats.Add(Name)
                Case HarplOPKind.StringVar
                    Strings.Add(Name)
                Case HarplOPKind.BoolVar
                    Booleans.Add(Name)
                Case Else
                    success = False
            End Select
            Return success
        End Function

        Public Function GetVariableKind(Name As String) As HarplOPKind
            If Strings.Contains(Name) Then
                Return HarplOPKind.StringVar
            ElseIf Floats.Contains(Name) Then
                Return HarplOPKind.FloatVar
            ElseIf Booleans.Contains(Name) Then
                Return HarplOPKind.BoolVar
            Else
                Return HarplOPKind.None
            End If

        End Function


    End Class
    Public GlobalScope As New Scope

End Class