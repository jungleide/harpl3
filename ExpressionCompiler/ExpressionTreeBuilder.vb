Imports Harpl.Compiler.Tokenizer

''' <summary>
''' Parses tokens that conform an evaluable expression and stops when end of expression, or end of tokens is found.
''' </summary>
Public Class ExpressionTreeBuilder

    ReadOnly _parent As ExpressionCompiler
    Private _curToken As Token
    Private _tokens As List(Of Token)
    Private _parentTokenIndex As Integer
    Public MyTokens As Token()

    Sub New(parent As ExpressionCompiler)
        _parent = parent
    End Sub

    Public Property IsSubexpression As Boolean
    Dim _parsingDone As Boolean = False
    Dim _lastIsIdentifier As Boolean = False
    Dim _lastIsOperator As Boolean = False
    Dim _parenthesisNesting As Integer = 0

    Public Function BuildTokensList(tokenIndex As Integer) As List(Of Token)
        _tokens = New List(Of Token)()
        _parentTokenIndex = tokenIndex
        _curToken = MyTokens(tokenIndex)

        'Rule: We can chain as many operators as we want, but we can't chain more than one identifier.

        _parsingDone = False
        _lastIsIdentifier = False
        _lastIsOperator = False
        _parenthesisNesting = 0
        While Not _parsingDone AndAlso Not EndOfTokens

            If _curToken.Kind = TokenKind.Comment Then
                NextToken()
                Continue While
            End If

            ProcessCurrentToken()

            If Not _parsingDone Then
                If _curToken.Kind <> TokenKind.Carrier Then _tokens.Add(_curToken)
                NextToken()
            End If
        End While

        Return _tokens
    End Function

    Private Sub ProcessCurrentToken()

        Select Case _curToken.Kind

            Case TokenKind.UnsemantedIdentifier, TokenKind.Number, TokenKind.StringLiteral, TokenKind.BoolLiteral
                ProcessUnsemantedIdentifier()

            Case TokenKind.And, TokenKind.Different, TokenKind.DivideOperator, TokenKind.Greater,
                TokenKind.GreaterEqual, TokenKind.MultiplyOperator, TokenKind.Equal, TokenKind.Or,
                TokenKind.Smaller, TokenKind.SmallerEqual, TokenKind.Concatenation
                ProcessBinaryOperators()

            Case TokenKind.AddOperator, TokenKind.Not, TokenKind.SubstractOperator
                ProcessUnnaryOperators()

            Case TokenKind.CloseParenthesis
                ProcessCloseParenthesis()

            Case TokenKind.OpenParenthesis
                ProcessOpenParenthesis()

            Case TokenKind.Coma
                ProcessComa()

            Case TokenKind.Carrier
                ProcessCarrier()

            Case Else
                _parsingDone = True

        End Select
    End Sub

    Private Sub ProcessUnnaryOperators()

        _lastIsOperator = True
        _lastIsIdentifier = False
    End Sub

    Private Sub ProcessCarrier()

        If _parenthesisNesting = 0 AndAlso _lastIsOperator = False Then _parsingDone = True
    End Sub

    Private Sub ProcessComa()

        If _parenthesisNesting = 0 Then
            _parentTokenIndex -= 1
            _parsingDone = True
        End If
        _lastIsOperator = True
        _lastIsIdentifier = False
    End Sub

    Private Sub ProcessOpenParenthesis()

        _lastIsOperator = True
        _lastIsIdentifier = False
        _parenthesisNesting += 1
    End Sub

    Private Sub ProcessCloseParenthesis()

        _lastIsOperator = False
        _lastIsIdentifier = True
        _parenthesisNesting -= 1
        If _parenthesisNesting < 0 Then
            _parsingDone = True
            If IsSubexpression = False Then _parent.CompilationResult.AddError("Unexpected closing parenthesis", _curToken.SourceLocation)
        End If
    End Sub

    Private Sub ProcessBinaryOperators()

        If _lastIsOperator Then _parsingDone = True
        _lastIsOperator = True
        _lastIsIdentifier = False
    End Sub

    Private Sub ProcessUnsemantedIdentifier()

        If _lastIsIdentifier Then _parsingDone = True
        _lastIsIdentifier = True
        _lastIsOperator = False
    End Sub

    Private Sub NextToken()
        If EndOfTokens() Then Exit Sub
        _parentTokenIndex += 1
        If Not EndOfTokens() Then
            _curToken = myTokens(_parentTokenIndex)
        Else
            _curToken = Nothing
        End If
    End Sub

    Private ReadOnly Property EndOfTokens As Boolean
        Get
            Return _parentTokenIndex >= myTokens.Length
        End Get
    End Property

    Public Function GetNextIndex() As Integer
        Return _parentTokenIndex + 1
    End Function


End Class