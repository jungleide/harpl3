﻿Imports System.Globalization

Public Module Utils

    Public Function StrToDouble(value As String) As Double
        Return Double.Parse(value, CultureInfo.InvariantCulture)
    End Function

End Module
