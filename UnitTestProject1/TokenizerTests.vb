﻿Imports Harpl.Compiler.Results
Imports Harpl.Compiler.Tokenizer

Imports NUnit.Framework

<TestFixture()> Public Class TokenizerTests
    <Test()> Sub TestTokenizer()
        Dim S As String = "Hello 12345+Hy/12.34>=welcome.45 0.4.5" & vbCr & "Hola 45 ""Esto es un literal entero dentro de un ~""código fuente~"""" \Comentario de la pradera!!" & vbCr & "Hola!"
        Dim tk As New Parser(S, New AssemblyCompilationResult())
        tk.Tokenize()
        For Each item In tk.Document.Tokens
            Console.WriteLine(">>" & item.Text & "<<" & item.SourceLocation.ToString & " " & item.Kind.ToString)
            Dim stringLiteralToken = TryCast(item, StringLiteralToken)
            If (stringLiteralToken IsNot Nothing) Then
                Console.WriteLine("ESCAPED:|" & stringLiteralToken.ScapedText & "|")
            End If
        Next
    End Sub
End Class
