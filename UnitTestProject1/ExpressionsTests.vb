﻿Option Explicit On
Option Strict On
Option Infer On

Imports Harpl.Compiler
Imports Harpl.Compiler.ByteCode
Imports NUnit.Framework

<TestFixture()> Public Class ExpressionsTests

    <Test> Public Sub TestExpressionBuilder()
        Const code = "Def MyVar:Float =  45 - Hola (45, 56) / 2 * MyOtherVar ++ ""Hello world!""/
                            2       >= 5.5 & \Esto es un comentario muy raruno
    Print ( ""Hello"" )  This + is/another Expression
"
        Dim comp As New HarplCompiler
        Dim result = comp.Compile(code)
        If result.Errors.Count > 0 Then
            MsgBox("There are compilation errors!")
        End If


        'Dim parser As New Parser(code)
        'Parser.Tokenize()
        'Dim expressionBuilder As New ExpressionCompiler(parser.Document, New IntermediateCode)
        'expressionBuilder.CompileExpression(0)
    End Sub

    <Test> Public Sub TextArtimeticASTPArser()
        Const code = "Def MyVar:Float = 2, Another:Float = 6 * (- MyVar)+1 * MyVar ,myVar2: Float = - Another, myStringVar:String = ""Hello world. Variable is: "" ++ Another"

        Dim comp As New HarplCompiler
        Dim result = comp.Compile(code)
        If result.Errors.Count > 0 Then
            MsgBox(result.GetErrorsDescription())
        End If
        Dim runner = New StringBasedInterpreter()
        runner.ByteCode = result.GeneratedIntermediateCode.Code
        runner.Run()
    End Sub

    <Test> Public Sub TestParenthesis()
        Const code = "Def var1:String = ""5"", var2:String = ""6"", var3:Bool = (var1 != var1)= !true, var4:Bool = ""A""<""B"""

        Dim comp As New HarplCompiler
        Dim result = comp.Compile(code)
        If result.Errors.Count > 0 Then MsgBox(result.GetErrorsDescription())
        Dim runner = New StringBasedInterpreter()
        runner.ByteCode = result.GeneratedIntermediateCode.Code
        runner.Run()
    End Sub

End Class

