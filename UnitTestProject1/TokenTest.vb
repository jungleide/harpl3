﻿Imports Harpl.Compiler.Results
Imports Harpl.Compiler.Tokenizer
Imports NUnit.Framework

<TestFixture()> Public Class TokenTest

    <Test()> Public Sub TokenComparisonTest()
        Dim t As New Token("hola adiós", 5, 2, New TextLocation(0, 5))
        Dim t2 As New Token("hola ADIÓS", 5, 2, New TextLocation(0, 5))
        Dim t3 As New Token("hola adiós", 6, 4, New TextLocation(0, 5))
        Assert.IsTrue(t = t2)
        Assert.IsTrue(t <> t3)
    End Sub

    <Test()> Public Sub TokenTextTest()
        Dim t As New Token("hola Grijalder", 5, 9, New TextLocation(0, 5))
        Assert.AreEqual(t.Text, "Grijalder")
    End Sub
End Class